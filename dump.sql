-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 27-Maio-2016 às 14:07
-- Versão do servidor: 5.5.47-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `pintinho`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nm_categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=35 ;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `nm_categoria`) VALUES
(1, 'GOVERNO ESTADUAL'),
(2, 'GOVERNO FEDERAL'),
(3, 'CONGRESSO NACIONAL'),
(4, 'ASSEMBLEIA LEGISLATIVA'),
(5, 'CAMARAS MUNICIPAIS - NITEROI'),
(6, 'CAMARAS MUNICIPAIS - SÃO GONÇALO'),
(7, 'CAMARAS MUNICIPAIS - MARICA'),
(8, 'CAMARAS MUNICIPAIS - ITABORAI'),
(9, 'CAMARAS MUNICIPAIS - GUAPIMIRIN'),
(10, 'CAMARAS MUNICIPAIS - CACHOEIRO'),
(11, 'CAMARAS MUNICIPAIS - RIO BONITO'),
(12, 'CAMARAS MUNICIPAIS - MAGE'),
(13, 'CAMARAS MUNICIPAIS - SILVA JARDIM'),
(14, 'PREFEITURAS DA REGIÃO - NITEROI'),
(15, 'PREFEITURAS DA REGIÃO - SÃO GONÇALO'),
(16, 'PREFEITURAS DA REGIÃO - MARICA'),
(17, 'PREFEITURAS DA REGIÃO - ITABORAI'),
(18, 'PREFEITURAS DA REGIÃO - GUAPIMIRIN'),
(19, 'PREFEITURAS DA REGIÃO - CACHOEIRO'),
(20, 'PREFEITURAS DA REGIÃO - TANGUA'),
(21, 'PREFEITURAS DA REGIÃO - RIO BONITO'),
(22, 'PREFEITURAS DA REGIÃO - MAGE'),
(23, 'PREFEITURAS DA REGIÃO - SILVA JARDIM'),
(24, 'ELEIÇÕES 2016 - NITEROI'),
(25, 'ELEIÇÕES 2016 - SÃO GONÇALO'),
(26, 'ELEIÇÕES 2016 - MARICA'),
(27, 'ELEIÇÕES 2016 - ITABORAI'),
(28, 'ELEIÇÕES 2016 - GUAPIMIRIN'),
(29, 'ELEIÇÕES 2016 - CACHOEIRO'),
(30, 'ELEIÇÕES 2016 - TANGUA'),
(31, 'ELEIÇÕES 2016 - RIO BONITO'),
(32, 'ELEIÇÕES 2016 - MAGE'),
(33, 'ELEIÇÕES 2016 - SILVA JARDIM'),
(34, 'CAMARAS MUNICIPAIS - TANGUA');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentarios`
--

CREATE TABLE IF NOT EXISTS `comentarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `nm_comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `txt_comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `post_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `comentarios_user_id_foreign` (`user_id`),
  KEY `comentarios_post_id_foreign_idx` (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_10_12_200000_create_categorias_table', 1),
('2014_10_12_200000_create_posts_table', 1),
('2014_10_12_300000_create_comentarios_table', 1),
('2016_05_18_185005_create_cidades_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `categoria_id` int(10) unsigned NOT NULL,
  `titulo_post` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `txt_post` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_post` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status_post` binary(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_user_id_foreign` (`user_id`),
  KEY `posts_categoria_id_foreign` (`categoria_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Extraindo dados da tabela `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `categoria_id`, `titulo_post`, `txt_post`, `imagem_post`, `status_post`, `created_at`, `updated_at`) VALUES
(15, 1, 1, 'Conselho da Petrobras avaliará indicação de Pedro Parente no dia 23', '<p><span style="color: #000000;">A Petrobras informou na manh&atilde; desta sexta-feira (20) que seu Conselho de Administra&ccedil;&atilde;o vai avaliar, durante reuni&atilde;o extraordin&aacute;ria agendada para segunda (23), a indica&ccedil;&atilde;o de Pedro Parente para o cargo de presidente da petroleira. At&eacute; o resultado da reuni&atilde;o do conselho, continua no cargo de presidente da estatal Aldemir Bendine, que est&aacute; no posto desde o ano passado, quando foi nomeado no governo da presidente afastada Dilma Rousseff.<strong> Em comunicado, a petroleira informou que o Conselho de Administra&ccedil;&atilde;o ir&aacute; apreciar tamb&eacute;m a indica&ccedil;&atilde;o de Parente para integrar o colegiado. Os atuais conselheiros foram eleitos em abril em assembleia de acionistas para um mandato de 2 anos. Dos 10 conselheiros, 7 foram indicados pelo governo Dilma, incluindo o atual presidente do conselho, Luiz Nelson Guedes de Carvalho.</strong></span></p>', '', '\0', '2016-05-20 00:14:46', '2016-05-24 21:57:11'),
(18, 1, 1, 'PF deflagra operação para investigar a Odebrecht e pessoas ligadas a Lula', '<p><span style="color: #000000;">A Pol&iacute;cia Federal (PF) deflagrou nesta sexta-feira (20) uma opera&ccedil;&atilde;o batizada de Janus que investiga contratos da construtora Odebrecht com o empres&aacute;rio Taiguara Rodrigues dos Santos, sobrinho da primeira mulher do ex-presidente Luiz In&aacute;cio Lula da Silva, um dos s&oacute;cios da empresa de constru&ccedil;&atilde;o civil de Santos (SP) Exergia Brasil. O petista n&atilde;o &eacute; foco direto das dilig&ecirc;ncias realizadas pelos policiais federais em S&atilde;o Paulo, em Santos e no Rio de Janeiro, mas &eacute; citado nas investiga&ccedil;&otilde;es. O Minist&eacute;rio P&uacute;blico Federal (MPF) afirma que a opera&ccedil;&atilde;o apura se Lula praticou tr&aacute;fico internacional de influ&ecirc;ncia em favor da Odebrechte facilitou ou agilizou a tramita&ccedil;&atilde;o de financiamentos de interesse da empreiteira junto ao Banco Nacional de Desenvolvimento Econ&ocirc;mico e Social (BNDES). A Opera&ccedil;&atilde', '', '\0', '2016-05-20 16:17:58', '2016-05-24 21:57:27'),
(19, 1, 1, 'Ministro da Justiça e ex-AGU participam juntos de evento em SC', '<p><span style="color: #000000;">Na manh&atilde; desta sexta-feira (20), o novo ministro da Justi&ccedil;a, Alexandre de Moraes (PSDB), e o ex-advogado geral da Uni&atilde;o (AGU) Jos&eacute; Eduardo Cardozo participaram em Florian&oacute;polis da formatura de uma turma do curso de forma&ccedil;&atilde;o profissional da Pol&iacute;cia Rodovi&aacute;ria Federal (PRF). Cardozo recebeu homenagem na cerim&ocirc;nia, pois ajudou a implementar a academia da PRF em Santa Catarina na &eacute;poca em que era ministro da Justi&ccedil;a. O evento ocorreu no Centro de Eventos de Canasvieiras, no Norte da Ilha. A academia da PRF recebe rec&eacute;m-aprovados em concurso da PRF do Brasil inteiro para uma forma&ccedil;&atilde;o inicial de tr&ecirc;s meses, com aulas pr&aacute;tica como defesa pessoal, tiro, dire&ccedil;&atilde;o e outras bem como aulas te&oacute;ricas de Direito, Ci&ecirc;ncias Humanas, etc. "Na solenidade est&atilde;o sendo formados 779 alunos que, ap&oacute;s a formatura, ser&atild', '', '\0', '2016-05-20 17:47:19', '2016-05-24 21:57:41'),
(20, 1, 20, 'Famílias de vítimas de voo da Egyptair esperam angustiadas por notícias', 'Diplomatas e responsáveis da companhia aérea se reuniram neste dia com as famílias, que estão hospedadas em um hotel no aeroporto internacional do Cairo, para tentar ajudá-las e responder suas perguntas.\r\nNa entrada do hotel Novotel, o embaixador francês no Eigot, André Parant, disse aos jornalistas que os parentes das vítimas - 15 das quais são francesas- "estão em choque e chorando".\r\nParant, que tanto na quinta quanto nesta sexta se encontrou com várias famílias das 66 pessoas que viajavam a bordo do Airbus A-320 de Paris ao Cairo, em sua maioria egípcias, reconheceu que são "momentos muito difíceis e dolorosos".\r\n"Fazem perguntas sobre o acidente, sobre as circunstâncias desta catástrofe, e por enquanto não podemos dar respostas precisas", acrescentou.', '', '\0', '2016-05-20 17:54:17', '2016-05-20 18:14:01'),
(21, 1, 11, 'Testand', '<p>testeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee</p><p>testeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee</p>\r\ntestandoooooooooooooooooooooooooooo <br>\r\n<b>tnegrito</b>', '', '\0', '2016-05-20 23:36:52', '2016-05-20 23:36:52'),
(22, 1, 20, 'Postagem de teste', '<p>Diplomatas e respons&aacute;veis da companhia a&eacute;rea se reuniram neste dia com as fam&iacute;lias, que est&atilde;o hospedadas em um hotel no aeroporto internacional...flash smite consumir</p>', '', '\0', '2016-05-21 00:46:54', '2016-05-24 22:03:48'),
(23, 1, 9, 'teste', '<p>dasadadasasdasd</p>', '73242.jpg', '\0', '2016-05-21 01:05:24', '2016-05-21 01:05:24'),
(25, 1, 1, 'teste de tudo', '<p><span style="color: #000000;">Vamos testar se essa porra pega preto mesmo!!!!!!!!!!!!!!</span></p>\r\n<p><span style="color: #ff0000;">Vamos testar se essa porra pega vermelho&nbsp;mesmo!!!!!!!!!!!!!!</span></p>\r\n<p><span style="color: #008000;">Vamos testar se essa porra pega verde&nbsp;mesmo!!!!!!!!!!!!!!</span></p>\r\n<p><span style="color: #0000ff;">Vamos testar se essa porra pega azul&nbsp;mesmo!!!!!!!!!!!!!!</span></p>\r\n<p><span style="color: #ff00ff;">Vamos testar se essa porra pega rosa&nbsp;mesmo!!!!!!!!!!!!!!</span></p>\r\n<p><strong><span style="color: #000000;">Vamos testar se essa porra pega preto mesmo!!!!!!!!!!!!!!</span></strong></p>\r\n<p><strong><span style="color: #ff0000;">Vamos testar se essa porra pega vermelho&nbsp;mesmo!!!!!!!!!!!!!!</span></strong></p>\r\n<p><strong><span style="color: #008000;">Vamos testar se essa porra pega verde&nbsp;mesmo!!!!!!!!!!!!!!</span></strong></p>\r\n<p><strong><span style="color: #0000ff;">Vamos testar se essa porra pega azul&nbsp;mesmo!!!', '73242.jpg', '\0', '2016-05-21 02:12:44', '2016-05-21 02:12:44'),
(26, 1, 1, 'TITULO ', '<p><span style="color: #000000;">Texto para bulicar</span></p>', 'left.png', '\0', '2016-05-24 20:40:50', '2016-05-24 20:40:50'),
(27, 1, 14, 'Teste novo', '<p><span style="color: #000000;">UEEEEEEEEEEEEEEEEEEEEEEEee&nbsp;</span></p>', '73242.jpg', '\0', '2016-05-26 23:06:18', '2016-05-26 23:06:18'),
(28, 1, 3, 'teste', '<p><span style="color: #000000;">SEEEEEEEEEEEEEEEEEEEEE</span></p>', '73242.jpg', '\0', '2016-05-26 23:15:46', '2016-05-26 23:15:46');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nivel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `nivel`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Renato', 'renato@gmail.com', '$2y$10$.KcDdCdmkZ7PRQmbH5sqAetV1t/QsNgjdW5GTn2gvT5LWPezKtWTC', '', 'cJOpz01cxP9qZa02Qnn2yu1oguDGIi8Ki5j7vym47KcURwljBZHfm011a808', NULL, '2016-05-26 22:24:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vereadores`
--

CREATE TABLE IF NOT EXISTS `vereadores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoria_id` int(10) unsigned NOT NULL,
  `nm_vereadores` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentarios_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `comentarios_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`),
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;