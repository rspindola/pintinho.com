<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->delete();

        DB::table('posts')->insert(
            ['user_id' => '1',
             'categoria_id' => '1',
             'titulo' => 'Duas visões: a “pílula do câncer” deve ser liberada no Brasil?',
             'txt_post' => 'Nesta quinta-feira, o Supremo Tribunal Federal julga uma ação da Associação Médica Brasileira (AMB) que questiona a lei que libera o porte, o uso, a distribuição e a fabricação da substância, supostamente eficaz no combate contra tumores.
            
            Sancionada pela presidente afastada Dilma Rousseff a poucos dias da votação do impeachment no Senado, o texto permite que pacientes diagnosticados com a doença usem a fosfoetanolamina por livre escolha.']
        );
    
        DB::table('posts')->insert(
            ['user_id' => '1',
             'categoria_id' => '1',
             'titulo' => 'Eletrobras diz que tem 10 dias para apresentar recurso na bolsa de NY',
             'txt_post' => 'A Eletrobras informou que tem prazo de dez dias úteis para encaminhar preliminares de recurso que pretende apresentar contra a decisão da Bolsa de Valores de Nova York (Nyse) de suspender a negociação dos papéis da companhia, que ocorreu a partir de quarta-feira (18).']
        );
    }
}
