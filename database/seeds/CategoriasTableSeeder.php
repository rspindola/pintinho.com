<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->delete();

        DB::table('categorias')->insert(['nm_categoria' => 'GOVERNO ESTADUAL']);
        DB::table('categorias')->insert(['nm_categoria' => 'GOVERNO FEDERAL']);
        DB::table('categorias')->insert(['nm_categoria' => 'CONGRESSO NACIONAL']);
        DB::table('categorias')->insert(['nm_categoria' => 'ASSEMBLEIA LEGISLATIVA']);
        
        DB::table('categorias')->insert(['nm_categoria' => 'CAMARAS MUNICIPAIS - NITEROI']);
        DB::table('categorias')->insert(['nm_categoria' => 'CAMARAS MUNICIPAIS - SÃO GONÇALO']);
        DB::table('categorias')->insert(['nm_categoria' => 'CAMARAS MUNICIPAIS - MARICA']);
        DB::table('categorias')->insert(['nm_categoria' => 'CAMARAS MUNICIPAIS - ITABORAI']);
        DB::table('categorias')->insert(['nm_categoria' => 'CAMARAS MUNICIPAIS - GUAPIMIRIN']);
        DB::table('categorias')->insert(['nm_categoria' => 'CAMARAS MUNICIPAIS - CACHOEIRO']);
        DB::table('categorias')->insert(['nm_categoria' => 'CAMARAS MUNICIPAIS - RIO BONITO']);
        DB::table('categorias')->insert(['nm_categoria' => 'CAMARAS MUNICIPAIS - MAGE']);
        DB::table('categorias')->insert(['nm_categoria' => 'CAMARAS MUNICIPAIS - SILVA JARDIM']);
        DB::table('categorias')->insert(['nm_categoria' => 'CAMARAS MUNICIPAIS - TANGUA']);

        
       DB::table('categorias')->insert(['nm_categoria' => 'PREFEITURAS DA REGIÃO - NITEROI']);
       DB::table('categorias')->insert(['nm_categoria' => 'PREFEITURAS DA REGIÃO - SÃO GONÇALO']);
       DB::table('categorias')->insert(['nm_categoria' => 'PREFEITURAS DA REGIÃO - MARICA']);
       DB::table('categorias')->insert(['nm_categoria' => 'PREFEITURAS DA REGIÃO - ITABORAI']);
       DB::table('categorias')->insert(['nm_categoria' => 'PREFEITURAS DA REGIÃO - GUAPIMIRIN']);
       DB::table('categorias')->insert(['nm_categoria' => 'PREFEITURAS DA REGIÃO - CACHOEIRO']);
       DB::table('categorias')->insert(['nm_categoria' => 'PREFEITURAS DA REGIÃO - TANGUA']);
       DB::table('categorias')->insert(['nm_categoria' => 'PREFEITURAS DA REGIÃO - RIO BONITO']);
       DB::table('categorias')->insert(['nm_categoria' => 'PREFEITURAS DA REGIÃO - MAGE']);
       DB::table('categorias')->insert(['nm_categoria' => 'PREFEITURAS DA REGIÃO - SILVA JARDIM']);
        
       DB::table('categorias')->insert(['nm_categoria' => 'ELEIÇÕES 2016 - NITEROI']);
       DB::table('categorias')->insert(['nm_categoria' => 'ELEIÇÕES 2016 - SÃO GONÇALO']);
       DB::table('categorias')->insert(['nm_categoria' => 'ELEIÇÕES 2016 - MARICA']);
       DB::table('categorias')->insert(['nm_categoria' => 'ELEIÇÕES 2016 - ITABORAI']);
       DB::table('categorias')->insert(['nm_categoria' => 'ELEIÇÕES 2016 - GUAPIMIRIN']);
       DB::table('categorias')->insert(['nm_categoria' => 'ELEIÇÕES 2016 - CACHOEIRO']);
       DB::table('categorias')->insert(['nm_categoria' => 'ELEIÇÕES 2016 - TANGUA']);
       DB::table('categorias')->insert(['nm_categoria' => 'ELEIÇÕES 2016 - RIO BONITO']);
       DB::table('categorias')->insert(['nm_categoria' => 'ELEIÇÕES 2016 - MAGE']);
       DB::table('categorias')->insert(['nm_categoria' => 'ELEIÇÕES 2016 - SILVA JARDIM']);
    }
}
