<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostsVereadoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vereador_posts', function(Blueprint $table){
        $table->unsignedInteger('post_id');
        $table->unsignedInteger('vereador_id');
        $table->foreign('post_id')->references('id')->on('posts');
        $table->foreign('vereador_id')->references('id')->on('vereadores');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
