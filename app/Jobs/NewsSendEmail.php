<?php

namespace App\Jobs;

use App\Jobs\Job;
use Auth;
use App\Model\Assinantes;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewsSendEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public function handle(Mailer $mailer)
    {
        $assinantes = Assinantes::all();

        $mailer->send('emails.demo', [], function($message) use ($assinantes){
            //$message->to('renato@live.com', 'Renato')->subject('Mensagem teste!');
            foreach ($assinantes as $key => $assinante) {
                if($key ==0){
                    $message->to($assinante->email_assinantes)->subject('Mensagem teste!');
                }else{
                    $message->bcc($assinante->email_assinantes);
                }
            }
        });
    }
}
