<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Request;

class ContactEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public function handle(Mailer $mailer)
    {
        $data = Request::all();

        $mailer->send('emails.contato', $data, function($message){
            $message->to('rspindola@live.com', 'Formulario de Contato')->subject('FeedBack do site');
        });
    }
}
