<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{
	protected $fillable = [
        'ip', 'agent'
    ];
    
    /**
     * Conta as visitas no site de acordo com a configuração passada
     *
     * @param  string $startDate = null [FORMATO: Y-m-d]
     * @param  string $endDate = null [FORMATO: Y-m-d]
     * @return integer $count
     */
    
    public static function countVisits($startDate = null, $endDate = null) {
        $startDate = (empty($startDate) ? date('d-m-Y') : $startDate);
        $endDate   = (empty($endDate) ? date('d-m-Y') . ' 23:59:59' : $endDate);
        
        return self::whereBetween('created_at', [$startDate, $endDate])->count();
    }

    public static function totalVisits() 
    {
        return  self::count();
    }
}
