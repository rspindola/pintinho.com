<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model,
App\User,
App\Model\Comentario,
App\Model\Post,
App\Model\Categorias;

class Vereadores extends Model
{
    protected $fillable = [     
        'categoria_id', 'nm_vereadores'
    ];

    public function vereador()
    {

    }

    public function posts()
    {
        return $this->belongsToMany(Post::class, "vereador_posts","vereador_id","post_id");
    }

    Public function user() 
    { 
         return $this->belongsTo(User::class);
    }
    
    public function comentarios()
    {
        return $this->hasMany(Comentario::class);
    }
    
    public function categoria()
    {
        return $this->belongsTo(Categorias::class);
    }
}
