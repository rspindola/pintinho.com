<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Assinantes extends Model
{
    protected $fillable = [
        'nm_assinantes', 'email_assinantes'
    ];
}
