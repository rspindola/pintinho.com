<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model,
App\User,
App\Model\Comentario,
App\Model\Categorias;

class Post extends Model
{
    protected $fillable = [
        'user_id','imagem_post', 'titulo_post', 'txt_post', 
        'categoria_id', 'status_post', 'sub_post', 'slug', 
        'destaque_post'
    ];

    Public function user() 
    { 
         return $this->belongsTo(User::class);
    }
    
    public function comentarios()
    {
        return $this->hasMany(Comentario::class);
    }
    
    public function categoria()
    {
        return $this->belongsTo(Categorias::class);
    }

    public function vereadores()
    {
        return $this->belongsToMany(Vereadores::class, "vereador_posts","post_id","vereador_id");
    }

    public static function findBySlug($slug)
    {
        $verificaSlug = self::whereSlug($slug)->first();
        
        if(empty($verificaSlug)) {
            abort(500, "Slug não encontrado!");
        }

        return $verificaSlug;
    }

    public static function getRecent($take) 
    {
        return  self::orderBy('created_at', 'desc')->take($take)->get();
    }

    public static function getMostCommented($take)
    {
        return self::selectRaw('posts.*, count(comentarios.id) as comments_num')->leftJoin('comentarios', 'post_id', '=', 'posts.id')
            ->orderBy('comments_num', 'desc')->groupBy('posts.id')->take($take)->get();
    }
}
