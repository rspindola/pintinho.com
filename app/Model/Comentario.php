<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model,
App\Model\Post;

class Comentario extends Model
{
   	 protected $fillable = [
        'user_id', 'nm_comentario', 'txt_comentario', 'created_at', 'updated_at', 'post_id'
    ];

    public function posts()
    {
       return $this->belongsToMany(Post::class);
    }

    public static function getRecent($take = 8) 
    {
        return  self::orderBy('created_at', 'desc')->take($take)->get();
    }
}
