<?php

namespace App\Http\Controllers;

use 
App\Http\Requests\PostFormRequest, 
Illuminate\Support\Facades\DB,
App\Jobs\NewsSendEmail, 
App\Http\Controllers\Controller,
App\Model\Post,
App\Model\Categorias,
App\Model\Comentario,
App\Model\Vereadores,
Response,
Auth,
Input,
Request;

class PostController extends Controller
{
    
    public function novo()
    {  
        $categorias = Categorias::lists('nm_categoria', 'id');
        return view('admin.post.novo', compact('categorias'));
    }
    
    public function mostra($slug)
    {         
        $post = Post::findBySlug($slug);
        $post->increment('views');

        return view('blog.post')->with('p', $post);
    }

    public function adiciona(PostFormRequest $request)
    {
        // pegar dados do formulario        
        $data = Request::except('_token');

        $imagem = str_random(10) . '-' .Request::file('imagem_post')->getClientOriginalName();
        Request::file('imagem_post')->move('assets/images/posts', $imagem);
        $data['imagem_post'] = $imagem;
        
        $post = Auth::user()->posts()->create($data);

        if($request->exists('vereador_id')) {
            $post->vereadores()->sync([$data["vereador_id"]]);
        }


        // retornar alguma view 
        alert()->success('Publicado com sucesso.', 'Concluido');
        return redirect()->action('PostController@lista');   
    }
    
    public function editar($id)
    {    
        $post = Post::find($id);
        $categorias = Categorias::lists('nm_categoria', 'id');

        return view('admin.post.editar', compact('post','categorias'));
    }
    
    public function update($id){
        
        if(Request::hasFile('imagem_post')){
            $imagem = str_random(10) . '-' .Request::file('imagem_post')->getClientOriginalName();
            Request::file('imagem_post')->move('assets/images/posts', $imagem);
            $data['imagem_post'] = $imagem;
        }else{
            $data = Request::except('_token, imagem_post');
        }


        Post::find($id)->update($data);
        alert()->success('Publicação alterada com sucesso.', 'Concluido');
        return redirect()->action('PostController@lista');
    }
    
    
    public function lista()
    {
        $posts = Post::all()->sortByDesc("create_at");
        return view('admin.post.listagem')->with('posts', $posts);    
    }
    
    public function remove($id)
    {
        $post = Post::find($id);
        $post->delete();
        alert()->success('Publicação deletada com sucesso.', 'Concluido');

        return redirect()->action('PostController@lista');
    }
    
    public function vereadores($id)
    {   
        $vereadores = Vereadores::orderBy('nm_vereadores')->where('categoria_id', '=', $id)->get();
                
        return view('blog.vereadores')->with('vereadores', $vereadores);
        
    }
    
    public function categorias($id)
    {   
        $posts = Post::orderBy('created_at', 'desc')->where('categoria_id', '=', $id)->paginate(4);
        if (empty($posts)){
            return "eita porar";
        }else{
            return view('blog.categoria')->with('posts', $posts);
        }
        
    }

    public function posts_veradores($id)
    {
        $posts = Vereadores::find($id)->posts()->orderBy('created_at', 'desc')->paginate(4);
        return view('blog.categoria')->with('posts', $posts);
    }


    public function getVereadores()
    {
        $cat_id = Request::input('cat_id');
        $vereadores = Vereadores::where('categoria_id', '=', $cat_id)->get();

        return Response::json($vereadores);
    }

    public function SendNewsEmail()
    {
        $this->dispatch(new NewsSendEmail());
        alert()->success('E-mails diários enviados com sucesso', 'Concluido');

        return redirect()->back();
    }
    
}