<?php

namespace App\Http\Controllers;
use
Illuminate\Support\Facades\DB,
App\Http\Controllers\Controller,
App\Model\Post,
App\Model\Comentario,
Auth,
Request;

class ComentarioController extends Controller
{	
	public function salvar($id)
    {
		// pegar dados do formulario 
		$data = Request::except('_token');
	    Post::find($id)->comentarios()->create($data);  
        alert()->success('Sua participação é importante', 'Obrigado pelo comentário');

	    // retornar alguma view 
	    return redirect()->back(); 
	}

	public function lista()
    {
        $comments = Comentario::all()->sortByDesc("create_at");

        return view('admin.comentario.listagem')->with('comments', $comments);    
    }

    public function remove($id)
	{
		$comment = Comentario::find($id);
        $comment->delete();

		return redirect()->action('ComentarioController@lista');
	}

}
