<?php

namespace App\Http\Controllers;

use 
Illuminate\Support\Facades\DB,
App\Http\Controllers\Controller,
App\Model\Post,
App\Model\Categorias,
App\Model\Comentario,
App\Model\Assinantes,
App\Jobs\ContactEmail, 
Auth,
Alert,
Mail,
Request;

use App\Http\Requests;

class BlogController extends Controller
{

	public function index()
	{
	    return view('blog.index');
	}

	public function saveAssinante()
	{		
	    // pegar dados do formulario 
		$data = Request::except('_token');
	    Assinantes::create($data); 
        alert()->success('Aguarde as novidades em breve!', 'E-mail cadastrado com sucesso.'); 

	    // retornar alguma view 
	    return redirect()->back(); 
	}


	public function postContato() 
	{
		$this->dispatch(new ContactEmail());
        alert()->success('Aguarde a resposta em breve!', 'E-mail enviado com sucesso.');
        return redirect()->back();
	}
}
