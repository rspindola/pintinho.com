<?php

namespace App\Http\Middleware;

use Closure,
App\Model\Visita;

class Visitas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('visited')) {
                // Adicionar registro de visita no banco
                $agent = $request->header('User-Agent');
                $ip = $request->ip();
                Visita::create(['ip' => $ip, 'agent' => $agent]);

            session()->put('visited', true);
        }

        return $next($request);
    }
}
