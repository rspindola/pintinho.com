<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
    
Route::auth();

Route::get('/login', function () {
    return view('auth.login');
});

Route::group(['middleware' => 'auth'], function() {
    Route::get('/dashboard', function () {return view('admin.dashboard');});
    Route::get('/email/news', 'PostController@SendNewsEmail');

    //ROTAS DO CRUD POSTAGEM
    Route::get('/post/novo', 'PostController@novo'); //tela de nova postagem    
    Route::post('post/adiciona', 'PostController@adiciona'); //adiciona o post
    Route::get('/post/get-vereadores', 'PostController@getVereadoresCamara'); //pega os vereadores pra salvar
    Route::get('/post/lista', 'PostController@lista');  //Lista os posts 
    Route::get('/post/editar/{id}', 'PostController@editar'); //tela de editar postagem
    Route::post('/post/salvar/{id}', 'PostController@update'); //salva a alteração
    Route::get('/post/remove/{id}', 'PostController@remove'); //deleta o post
});

Route::group(['middleware' => 'visits'], function() {

    //ROTAS DOS COMENTARIOS
    Route::post('/comentario/salvar/{id}', 'ComentarioController@salvar');
    Route::get('/comment/lista', 'ComentarioController@lista');  //Lista os posts 
    Route::get('/comment/remove/{id}', 'ComentarioController@remove'); //deleta o post

    //ROTAS DO NEWSLETTER
    Route::post('salvarAssinantes', 'BlogController@saveAssinante');

    //ROTAS DO BLOG 
    Route::get('/', 'BlogController@index');
    Route::get('about', function () {return view('blog.about');});
    Route::get('vereadores', function () {return view('blog.vereadores');});
    Route::post('/contato', 'BlogController@postContato');
    Route::get('/categoria/{id}', 'PostController@categorias');
    Route::get('/categoria/vereadores/{id}', 'PostController@Vereadores');
    Route::get('/categoria/vereadores/posts/{id}', 'PostController@posts_veradores');
    Route::get('/ajax-subcat', 'PostController@getVereadores');
    Route::get('/post/{slug}', 'PostController@mostra');

});