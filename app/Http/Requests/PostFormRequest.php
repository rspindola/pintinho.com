<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Response;

class PostFormRequest extends FormRequest
{
    // Aqui você escreve as regras de validação de cada campo, tem lá na parte de validation do laravel
    protected $rules = [
        'titulo_post'             => 'required | max:300',
        'sub_post'                => 'required | max:500',
        'destaque_post'           => 'required | max:144',
        'txt_post'                => 'required | max:10000',
        'imagem_post'             => 'required'


    ];

    public function rules()
    {
        // Aqui nós podemos testar se o cara tá adicionando ou editando um campeonato, para mudar as regras se preciso.
        return $this->rules;
    }


    public function authorize()
    {
        return true;
    }

    public function forbiddenResponse()
    {
        return Response::make('Permission denied foo!', 403);
    }

}