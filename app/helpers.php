<?php

function getRecentPosts($take) {
    return App\Model\Post::getRecent($take);
}

function getMostCommentdPosts($take) {
    return App\Model\Post::getMostCommented($take);
}

function getRecentComments($take) {
    return App\Model\Comentario::getRecent($take);
}

function getTotalVisits(){
	return App\Model\Visita::totalVisits();
}