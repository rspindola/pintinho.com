@extends('layouts.blog')
@section('content')
<!--about-starts-->
	<div class="about">
		<div class="container">
			<div class="about-main">
				<div class="col-md-8 about-left">
					<h1>Não existem postagens</h1>
				</div>
				@include('layouts.lateral')		
			</div>		
		</div>
	</div>
@endsection