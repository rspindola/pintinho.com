<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head
		content must come *after* these tags -->
		<title>Pintinho.com</title>
		<!-- Bootstrap -->
		<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-social.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/css/sweetalert.css') }}">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media
		queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file://-->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!--header-top-starts-->
		<div class="header-top">
			<div class="container">
				<div class="col-md-5 col-sm-12 logo">
                    <a href="{{ url('/') }}"><img src="{{ asset('assets/images/logo-1.png') }}" </img></a>
				</div>
				<div class="col-md-offset-2 col-md-5 col-sm-12">
					<div id="custom-search-input">
						<div class="input-group col-md-12">
							<input type="text" class="form-control input-lg" placeholder="Buscar"
							/>
							<span class="input-group-btn">
								<button class="btn btn-info btn-lg" type="button">
									<i class="glyphicon glyphicon-search">
									</i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--header-top-end-->
		<!--start-header-->
		<div class="header">
			<div class="head">
				<nav class="navbar navbar-default menu">
					<div class="navbar-header">
						<a class="navbar-brand hidden-lg" href="#">MENU</a>
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
							<span class="icon-bar">
							</span>
							<span class="icon-bar">
							</span>
							<span class="icon-bar">
							</span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav navegacao">
							<li>
								<a href="{{ url('/') }}" class="active">Inicio</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Governo<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li>
										<a href="{{ url('/categoria/1') }}">Estadual</a>
									</li>
									<li>
										<a href="{{ url('/categoria/2') }}">Municipal</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="{{ url('/categoria/3') }}">Congresso Nacional</a>
							</li>
							<li>
								<a href="{{ url('/categoria/4') }}">Assembleia Legislativa</a>
							</li>
                            <li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Camaras Municipais
                                    <b class="caret"></b>
                                </a>
								<ul class="dropdown-menu">
									<li>
										<a href="{{ url('/categoria/vereadores/10') }}">Cachoeiro</a>
									</li>
									<li>
										<a href="{{ url('/categoria/vereadores/9') }}">Guapimirin</a>
									</li>
									<li>
										<a href="{{ url('/categoria/vereadores/8') }}">Itaboraí</a>
									</li>
                                    <li>
									   <a href="{{ url('/categoria/vereadores/12') }}">Magé</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/vereadores/7') }}">Maricá</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/vereadores/5') }}">Niterói</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/vereadores/11') }}">Rio Bonito</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/vereadores/6') }}">São Gonçalo</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/vereadores/13') }}">Silva Jardim</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/vereadores/4') }}">Tanguá</a>
									</li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Prefeituras das Regiões
                                    <b class="caret"></b>
                                </a>
								<ul class="dropdown-menu">
									<li>
										<a href="{{ url('/categoria/19') }}">Cachoeiro</a>
									</li>
									<li>
										<a href="{{ url('/categoria/18') }}">Guapimirin</a>
									</li>
									<li>
										<a href="{{ url('/categoria/17') }}">Itaboraí</a>
									</li>
                                    <li>
									   <a href="{{ url('/categoria/22') }}">Magé</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/16') }}">Maricá</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/14') }}">Niterói</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/21') }}">Rio Bonito</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/15') }}">São Gonçalo</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/23') }}">Silva Jardim</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/20') }}">Tanguá</a>
									</li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Eleições 2016
                                    <b class="caret"></b>
                                </a>
								<ul class="dropdown-menu">
									<li>
										<a href="{{ url('/categoria/29') }}">Cachoeiro</a>
									</li>
									<li>
										<a href="{{ url('/categoria/28') }}">Guapimirin</a>
									</li>
									<li>
										<a href="{{ url('/categoria/27') }}">Itaboraí</a>
									</li>
                                    <li>
									   <a href="{{ url('/categoria/32') }}">Magé</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/26') }}">Maricá</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/24') }}">Niterói</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/31') }}">Rio Bonito</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/25') }}">São Gonçalo</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/33') }}">Silva Jardim</a>
									</li>
                                    <li>
										<a href="{{ url('/categoria/30') }}">Tanguá</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="contents">
			@yield('content')
		</div>
		<!--footer-starts-->
		<div class="footer">
			<div class="container">
				<div class="footer-text">
					<p>
						© 2016 Paulo Pintinho. Todos os direitos Reservados | Design by
						<a href="http://www.renatospindola.com.br" target="_blank">RenatoSpíndola</a>
					</p>
				</div>
			</div>
		</div>
		<!--footer-end-->
		
		<!--FACEBOOK -->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.7";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
		<!--TWITTER-->
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
		</script>

		<!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="{{ asset('assets/MA/jquery/vendors/bower_components/jquery/dist/jquery.min.js') }}"></script>
	    <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-80141425-1', 'auto');
		  ga('send', 'pageview');

		</script>
		<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
		<!-- Include this after the sweet alert js file -->
		@include('vendor/sweet/alert')
	</body>

</html>