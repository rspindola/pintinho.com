<div class="col-md-4 about-right heading">
    <div class="abt-1">
        <h3>Sobre</h3>
        <div class="abt-one">
            <img class="foto-perfil" src="{{ asset('assets/images/about.png') }}" alt="foto perfil" />
            <p>Quisque non tellus vitae mauris luctus aliquam sit amet id velit. Mauris ut dapibus nulla, a dictum neque.</p>
            <div class="a-btn">
                <a href="/about">Saiba Mais</a>
            </div>
        </div>
    </div>
    <div class="abt-2">
        <h3>Entre em contato</h3>
            <div class="abt-2">
                <a href="http://www.facebook.com.br" target="_blank" class="btn btn-social-icon btn-facebook">
                    <span class="fa fa-facebook"></span>
                </a>
                <a href="http://www.twitter.com.br" target="_blank" class="btn btn-social-icon btn-twitter">
                    <span class="fa fa-twitter"></span>
                </a>
                <a href="http://www.youtube.com.br" target="_blank" class="btn btn-social-icon btn-pinterest">
                    <span class="fa fa-youtube"></span>
                </a>
                <a data-toggle="modal" data-target="#contactModal" class="btn btn-social-icon btn-tumblr">
                    <span class="fa fa-envelope-o"></span>
                </a>
            </div>
    </div>
    <div class="abt-2">
        <h3>Talvez você goste</h3>
        @foreach (getMostCommentdPosts(3) as $mv)
        <div class="might-grid">
            <div class="grid-might">
                <a href="/post/{{$mv->slug}}">
                <img src="{{ asset('assets/images/posts/' . $mv->imagem_post) }}"  class="img-responsive" alt=""> </a>
            </div>
            <div class="might-top">
                <h4><a href="/post/{{$mv->slug}}">{{$mv->titulo_post}}</a></h4>
                <p>{{$mv->destaque_post}}</p> 
            </div>
            <div class="clearfix"></div>
        </div>
        @endforeach						
    </div>
    <div class="abt-2">
        <h3>Comentários Recentes</h3>
        <ul>
            @foreach(getRecentComments(10) as $uc)
            <li><a href="/post/{{$uc->slug}}">{!! str_limit($uc->txt_comentario, $limit=80, $end="...")!!}</a></li>
            @endforeach
        </ul>	
    </div>
    <div class="abt-2">
        <h3>Receba novidades</h3>
        <div class="news">
            <form action="salvarAssinantes" method="post">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <input  class="espaco"
                        name="nm_assinantes"
                        id="nm_assinantes" 
                        type="text" 
                        maxlength="255" 
                        placeholder="Digite seu nome."
                        required="required" 
                />
                <input  class="espaco"
                        type="text" 
                        name="email_assinantes"
                        id="email_assinantes" 
                        placeholder="Digite seu e-mail"
                        required="required"  
                        maxlength="255" 
                />
                <input type="submit" value="Assine">
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<div id="contactModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Envie um e-mail</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <form action="/contato" method="post">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 10px;">
                <input  class="form-control" 
                        name="contact_nome" 
                        placeholder="Nome" 
                        type="text" 
                        required autofocus />
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 10px;">
                <input  class="form-control" 
                        name="contact_sobrenome" 
                        placeholder="Sobrenome" 
                        type="text" 
                        required />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                <input  class="form-control" 
                        name="contact_email" 
                        placeholder="E-mail" 
                        type="email" 
                        required />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                <input  class="form-control" 
                        name="contact_assunto" 
                        placeholder="Assunto" 
                        type="text" 
                        required />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <textarea style="resize:vertical;" class="form-control" placeholder="Mensagem..." rows="6" name="contact_text" required></textarea>
            </div>
        </div>
    </div>  
    <div class="panel-footer" style="margin-bottom:-14px;">
        <input type="submit" class="btn btn-success" value="Enviar"/>
            <!--<span class="glyphicon glyphicon-ok"></span>-->
        <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Cancelar</button>
    </div>
    </form>
</div>