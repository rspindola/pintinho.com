
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Pintinho.com</title>
    
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/sweetalert.css') }}">
    <!-- MetisMenu CSS -->
    <link rel="stylesheet" href="{{ asset('assets/MA/jquery/css/app.min.1.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/MA/jquery/css/app.min.2.css') }}">
    
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('assets/MA/jquery/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/MA/jquery/vendors/bower_components/animate.css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/MA/jquery/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/MA/jquery/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/MA/jquery/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}">

    <!--JQUERY-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>  
</head>

<body>    
<header id="header" class="clearfix" data-current-skin="blue">
        <ul class="header-inner">
            <li id="menu-trigger" data-trigger="#sidebar">
                <div class="line-wrap">
                    <div class="line top"></div>
                    <div class="line center"></div>
                    <div class="line bottom"></div>
                </div>
            </li>

            <li class="logo hidden-xs">
                <a href="http://www.blogdopintinho.com.br" target="_blank">Pintinho.com</a>
            </li>

            <li class="pull-right">
                <ul class="top-menu">
                    <li id="top-search">
                        <a href=""><i class="tm-icon zmdi zmdi-search"></i></a>
                    </li>
                </ul>
            </li>
        </ul>


        <!-- Top Search Content -->
        <div id="top-search-wrap">
            <div class="tsw-inner">
                <i id="top-search-close" class="zmdi zmdi-arrow-left"></i>
                <input type="text">
            </div>
        </div>
    </header>

    
    <section id="main" data-layout="layout-1">
        <aside id="sidebar" class="sidebar c-overflow">
            <div class="profile-menu">
                <a href="">
                    <div class="profile-pic">
                        <img src="img/profile-pics/1.jpg" alt="">
                    </div>

                    <div class="profile-info">
                        {{ Auth::user()->name }}
                        <i class="zmdi zmdi-caret-down"></i>
                    </div>
                </a>

                <ul class="main-menu">
                    <li>
                        <a href=""><i class="zmdi zmdi-settings"></i> Configurações</a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}"><i class="zmdi zmdi-time-restore"></i> Sair</a>
                    </li>
                </ul>
            </div>

            <ul class="main-menu">
                <li class="active">
                    <a href="{{ url('/dashboard') }}"><i class="zmdi zmdi-home"></i> Home</a>
                </li>
                <li class="sub-menu">
                    <a href=""><i class="zmdi zmdi-view-compact"></i> Publicação</a>

                    <ul>
                        <li><a href="{{ url('/post/novo') }}">Nova</a></li>
                        <li><a href="#">Rascunhos</a></li>
                        <li><a href="{{ url('/post/lista') }}">Listar</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ url('/comment/lista') }}"><i class="zmdi zmdi-comments"></i> Comentários</a>
                </li>
                <li>
                    <a href="{{ url('/email/news') }}"><i class="zmdi zmdi-comments"></i> Enviar E-mail</a>
                </li>
            </ul>
        </aside>

         @yield('content')
        
    </section>
    
            <footer id="footer">
            Copyright &copy; 2015 Material Admin
            
            <ul class="f-menu">
                <li><a href="">Home</a></li>
                <li><a href="">Dashboard</a></li>
                <li><a href="">Reports</a></li>
                <li><a href="">Support</a></li>
                <li><a href="">Contact</a></li>
            </ul>
        </footer>

        <!-- Page Loader -->
        <div class="page-loader">
            <div class="preloader pls-blue">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>

                <p>Aguarde um momento</p>
            </div>
        </div>

        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
        <!-- Include this after the sweet alert js file -->
        @include('vendor/sweet/alert')

        <!-- Javascript Libraries -->
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>    
        
        <script src="{{ asset('assets/MA/jquery/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/vendors/bower_components/flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/vendors/bower_components/flot/jquery.flot.resize.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/vendors/bower_components/flot.curvedlines/curvedLines.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/vendors/sparklines/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>

        
        <script src="{{ asset('assets/MA/jquery/vendors/bower_components/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/vendors/bower_components/Waves/dist/waves.min.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/vendors/bootstrap-growl/bootstrap-growl.min.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>

        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        
        <script src="{{ asset('assets/MA/jquery/js/flot-charts/curved-line-chart.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/js/flot-charts/line-chart.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/js/charts.js') }}"></script>

        <script src="{{ asset('assets/MA/jquery/js/charts.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/js/functions.js') }}"></script>
        <script src="{{ asset('assets/MA/jquery/js/demo.js') }}"></script>
        
    </body>
  </html>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

