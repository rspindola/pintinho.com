@extends('layouts.blog')
@section('content')
<div class="about">
        <div class="container">
            <div class="about-main">
                <div class="col-md-8 about-left">
                    <div class="about-tre">
                        @foreach(getRecentPosts(8) as $post)
                        <div class="abt-left">
                            <div class='row'>
                                <div class="col-xs-12 col-md-6">
                                    <a href="/post/{{$post->slug}}">
                                    <img class="thumbnail img-responsive" 
                                         src="{{ asset('assets/images/posts/' . $post->imagem_post) }}" 
                                         alt="imagem_post" />
                                    </a>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-7">
                                            <a href="/categoria/{{$post->categoria_id}}">
                                                <h6>{{$post->categoria->nm_categoria}}</h6>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 col-md-5">
                                            <label>{{$post->created_at->format("d/m/Y")}}</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3><a href="/post/{{$post->slug}}">{{$post->titulo_post}}</a></h3>
                                        </div>
                                        <div class=" about-two col-md-12">
                                            <p>{{$post->destaque_post}}</p>
                                        </div>
                                    </div>
                                    <div class="about-two">
                                        <ul>
                                                <div    
                                                    class="fb-share-button" 
                                                    data-href="http://www.blogdopintinho.com.br/post/{{$post->slug}}" 
                                                    data-layout="button" 
                                                    >
                                                    <a class="fb-xfbml-parse-ignore" 
                                                    target="_blank" 
                                                    href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.blogdopintinho.com.br%2F&amp;src=sdkpreparse">
                                                    Compartilhar
                                                    </a>
                                                </div>
                                            <li>
                                                <a  href="https://twitter.com/share" 
                                                    class="twitter-share-button" 
                                                    data-text="{{$post->titulo_post}}" 
                                                    data-lang="pt" 
                                                    data-dnt="true">
                                                    Tweetar
                                                </a>
                                            </li>
                                        </ul>
                                    </div>  
                                </div>  
                            </div>            
                        </div>
                        <hr class="style1"> 
                        @endforeach
                    </div>
                </div>
                @include('layouts.lateral')     
                <div class="clearfix"></div>            
            </div>      
        </div>
    </div>
@endsection

<script>
function shareSocial(provider, shareLink, text) {
    
    var link;
    
    switch(provider){
        case 'facebook':
            link = 'https://www.facebook.com/sharer.php?u=' + shareLink;
            break;
        case 'twitter':
            link = "https://twitter.com/share?url="+ shareLink + "&text=" + text + "&hashtags=";
            break;
        case 'google+':
            link = "https://plus.google.com/share?url=" + shareLink;
            break;
    }

    window.open(link, 'Compartilhar', 'height=640, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
}
</script>