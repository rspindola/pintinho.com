@extends('layouts.blog')
@section('content')
<div class="about">
    <div class="container">
        <div class="about-main">
            <div class="col-md-8 about-left">
                @foreach ($posts as $p)
                <div class="about-one">
                    <a href="/post/{{$p->slug}}"><h3>{{$p->titulo_post}}</h3</a>
                </div>
                <div class="about-two">
                    <div class="row">
                        <a href="/post/{{$p->slug}}">
                        <img class="img-responsive" src="{{ asset('assets/images/posts/' . $p->imagem_post) }}" 
                              alt="imagem_post" />
                    </a>
                    </div>
                    <p>Postado por <a href="#"><b>{{$p->user->name}}</a></b>, 
                        {{$p->created_at->format("d/m/Y à\s H:i:s")}}
                        <b><a href="#">comentários({{$p->comentarios()->count()}})</a></b>
                    </p>
                        <p>{!! str_limit($p->txt_post, $limit = 300, $end = '...') !!}</p>
                    <div class="row">
                        <div class="about-btn col-md-4">
                            <a href="/post/{{$p->slug}}">Saiba mais</a>
                        </div>
                        <div class="col-md-4 col-md-offset-4">
                            <ul>
                                <div    
                                    class="fb-share-button" 
                                    data-href="http://www.blogdopintinho.com.br/post/{{$p->slug}}" 
                                    data-layout="button" 
                                    >
                                    <a class="fb-xfbml-parse-ignore" 
                                    target="_blank" 
                                    href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.blogdopintinho.com.br%2F&amp;src=sdkpreparse">
                                    Compartilhar
                                    </a>
                                </div>
                            <li>
                                <a  href="https://twitter.com/share" 
                                    class="twitter-share-button" 
                                    data-text="{{$p->titulo_post}}" 
                                    data-lang="pt" 
                                    data-dnt="true">
                                    Tweetar
                                </a>
                            </li>
                        </ul>
                        </div>
                    </div>
                </div>
                <hr class="style1">
                @endforeach
                {!! $posts->links() !!}
            </div>
            @include('layouts.lateral')		
            <div class="clearfix"></div>			
        </div>		
    </div>
</div>
@endsection

<script>
function shareSocial(provider, shareLink, text) {
    
    var link;
    
    switch(provider){
        case 'facebook':
            link = 'https://www.facebook.com/sharer.php?u=' + shareLink;
            break;
        case 'twitter':
            link = "https://twitter.com/share?url="+ shareLink + "&text=" + text + "&hashtags=";
            break;
        case 'google+':
            link = "https://plus.google.com/share?url=" + shareLink;
            break;
    }

    window.open(link, 'Compartilhar', 'height=640, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
}
</script>