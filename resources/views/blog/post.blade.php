@extends('layouts.blog')
@section('content')
<!--start-single-->
	<div class="single">
		<div class="container">
            <div class="col-md-8">
                <div class="single-top">
                    <ul class="blog-ic">
                        <li>
                            <span>
                                Publicado {{$p->created_at->format("d/m/Y à\s H:i:s")}} 
                                por {{$p->user->name}} || 
                            </span>
                            <span>
                                {{$p->views}} visualizações
                            </span>
                        </li>
                    </ul>           
                    <h4 class="titulo-single">{{$p->titulo_post}}</h4>
                    <h4 class="subtitulo-single">{{$p->sub_post}}</h4>
                    <img class="img-responsive" 
                         src="{{ asset('assets/images/posts/' . $p->imagem_post) }}" 
                         alt="imagem_post"></img>
                    <div class="single-grid">
                        <p>{!! $p->txt_post !!}</p>	
                    </div>
                    <div class="about-two">
                        <div class="col-md-12">
                        O que você achou?
                            <button href="" id="like-btn" class="rate-btn btn btn-sm btn-hover btn-success">
                                <span class="glyphicon glyphicon-thumbs-up"></span>  Gostei
                            </button>
                            <button href="#" id="deslike-btn" class="rate-btn btn btn-sm btn-hover btn-danger">
                                <span class="glyphicon glyphicon-thumbs-down"></span>  Não gostei
                            </button>
                        </div>
                        <div class="col-md-12">
                            Compartilhe: 
                                <div    
                                    class="fb-share-button" 
                                    data-href="http://www.blogdopintinho.com.br/post/{{$p->slug}}" 
                                    data-layout="button" 
                                    >
                                    <a class="fb-xfbml-parse-ignore" 
                                    target="_blank" 
                                    href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.blogdopintinho.com.br%2F&amp;src=sdkpreparse">
                                    Compartilhar
                                    </a>
                                </div>
                            
                                <a  href="https://twitter.com/share" 
                                    class="twitter-share-button" 
                                    data-text="{{$p->titulo_post}}" 
                                    data-lang="pt" 
                                    data-dnt="true">
                                    Tweetar
                                </a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="comments heading">
                        <h3>Comentarios</h3>
                        @if($p->comentarios()->exists())
                            @foreach($p->comentarios as $comentario)
                            <div class="media">
                                <div class="media-body">
                                    <h4 class="media-heading">{{$comentario->nm_comentario}}</h4>
                                    <p>{!! $comentario->txt_comentario !!}</p>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <div class="media">
                                <div class="media-body">
                                    <h4 class="media-heading">Seja o Primeiro a comentar</h4>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="comment-bottom heading">
                        <h3>Deixe um comentário</h3>
                        <form action="/comentario/salvar/{{$p->id}}" method="post">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                            <input 
                                type="text" 
                                name="nm_comentario" 
                                maxlength="30" 
                                placeholder="Digite seu Nome" 
                                required 
                            />
                            <textarea 
                                name="txt_comentario" 
                                rows="6" 
                                placeholder="Digite seu comentário"></textarea>

                            <button name="publicar" type="submit" class="btn btn-success">Publicar</button>
                        </form>
                    </div>
                </div>
            </div>	
            @include('layouts.lateral')     
        </div>
	</div>
	<!--end-single-->
@endsection

<script type="text/javascript" src='//cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
    tinymce.init({  
        selector: 'textarea',  
        menu: {},
        auto_focus: 'txt_post',
        height: 300,
        plugins: [
            'advlist autolink textcolor lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: ' ',
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
</script>