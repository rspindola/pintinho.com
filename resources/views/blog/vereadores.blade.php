@extends('layouts.blog')
@section('content')
<div class="container">
	<div class="team">
		<div class="container">
		<div class="team-top heading">
			<h3>VEREADORES</h3>
		</div>
			<div class="team-bottom">
				@foreach ($vereadores as $v)
                <div class="col-md-3 team-left">
					<a href="/categoria/vereadores/posts/{{$v->id}}">
						<img src="{{ asset('assets/images/vere1.png') }}" alt="foto vereador" />
						<h4>{{$v->nm_vereadores}}</h4>
						<p>{{$v->desc_vereadores}}</p>
					</a>
				</div>
                @endforeach
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
@endsection