<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <title>kreative</title>
    <!--[if gte mso 12]>
    > <style type="text/css">
    > [a.btn {
        padding:15px 22px !important;
        display:inline-block !important;
    }]
    > </style>
    > <![endif]-->
</head>
<body>
    <body bgcolor="#f6f6f6" style="font-family: Arial; background-color: #f6f6f6;">

<table width="630" class="container" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table align="left">
				<tr>
					<td width="188" class="Logo">
						<img src="http://www.rickygipson.com/images/codepen/company_logo.png">					
					</td>
				</tr>
			</table>
			<table align="right">
				<tr>
					<td height="70" class="viewWebsite">
						<p style="font-family: Arial, Helvetica, sans-serif; color: #555555; font-size: 10px; padding: 0; margin: 0;">If you are having trouble viewing this email, please <a href="#" style="color: #990000;">click here</a>.</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table width="630" bgcolor="#fcfcfc" style="border: 1px solid #dddddd; line-height: 135%;" class="container" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td bgcolor="#fcfcfc" colspan="3" width="100%" height="10">&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#fcfcfc" colspan="3" align="center">
			<img src="http://www.rickygipson.com/images/codepen/main-image.jpg" width="100%">
		</td>
	</tr>
	<tr>
		<td colspan="3" height="15">&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#fcfcfc" colspan="3">
			<table>
				<tr>
					<td width="30" class="spacer">&nbsp;</td>
					<td align="center" class="bodyCopy">
						<h1 style="font-family: Arial, Helvetica, sans-serif; font-size: 32px; color: #404040; margin-top: 0; margin-bottom: 20px; padding: 0; line-height: 135%" class="headline">This is an awesome headline</h1>
						<p style="font-family: Arial, Helvetica, sans-serif; color: #555555; font-size: 14px; padding: 0 40px;">Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, some link quis, varius in, purus.</p>
					</td>
					<td width="30" class="spacer">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<img src="http://www.rickygipson.com/images/codepen/grey_div.jpg" width="95%">
		</td>
	</tr>
	<tr>
		<td colspan="3" height="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" class="force-width">
            @foreach (getMostCommentdPosts(3) as $mv)
			<table width="213" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="belowConsoles">
				<tr>
					<td width="35" class="spacer"></td>
					<td>
						<div class="consoleImage">
						<img src="{{ asset('assets/images/posts/' . $mv->imagem_post) }}" class="belowFeatureIMG">
					    </div>
						<h3 style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #404040; margin: 0; padding: 0;">
							{{$mv->titulo_post}}
						</h3>
						<p style="font-family: Arial, Helvetica, sans-serif; color: #555555; font-size: 13px;">
							{{$mv->destaque_post}}
						</p>
						<a href="/post/{{$mv->slug}}">
							<button></button>
						</a>
					</td>
				</tr>
			</table>
            @endforeach
		</td>
	</tr>
</table>


</body>
</body>
</html>