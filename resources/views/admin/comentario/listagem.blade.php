@extends('layouts.dash')
@section('content')
<section id="content">
    <div class="container-fluid">
        <div class="card">            
            <div class="col-md-offset-1 col-md-10">
                <div class="card-header">
                    <h2>Listagem de Comentários <small>Listagem com todas os comentários publicados.</small></h2>
                </div>
                <div class="table-responsive">
                    <table id="data-table-basic" class="table table-striped">
                        <thead>
                            <tr>
                                <th data-column-id="id" data-type="numeric">Nome</th>
                                <th data-column-id="sender">Comentário</th>
                                <th data-column-id="data" data-order="desc">Data</th>
                                <th>Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($comments as $c)
                            <tr>
                                <td>{{$c->nm_comentario}}</td>
                                <td>{!! str_limit($c->txt_comentario, $limit = 30, $end = '...') !!}</td>
                                <td>{{$c->created_at->format("d/m/Y à\s H:i:s")}}</td>
                                <td> 
                                    <a href="/post/{{$c->slug}}"> 
                                        <i class="zmdi zmdi-search zmdi-hc-fw"></i>
                                    </a> 
                                    @if(Auth::id() == 1)
                                    <a href="#" data-id="{{ $c->id }}" class="delete-comment">
                                            <i class="zmdi zmdi-delete zmdi-hc-fw"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                                <tr class="alert alert-danger">
                                    Você não tem nenhuma publicação.
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="delete-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalLabel">Excluir Item</h4>
          </div>
          <div class="modal-body">
            Deseja realmente excluir este item?
          </div>
          <div class="modal-footer">
            <a id="botao-modal" href="/comment/remove/{{$c->id}}" type="button" class="btn btn-primary">Sim</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
          </div>
        </div>
    </div>
</div> 
<script>
    $('.delete-comment').click(function(){ 
    
    $('#botao-modal').prop('href', "/comment/remove/" + $(this).data('id'));
        $("#delete-modal").modal();
    }); 
</script>
<!-- /.modal -->
@endsection

