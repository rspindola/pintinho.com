@extends('layouts.dash')

@section('content')        
<section id="content">
    <div class="container">
        <div class="block-header">
            <h2>Dashboard</h2>

            <ul class="actions">
                <li>
                    <a href="">
                        <i class="zmdi zmdi-trending-up"></i>
                    </a>
                </li>
                <li>
                    <a href="">
                        <i class="zmdi zmdi-check-all"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="" data-toggle="dropdown">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="">Refresh</a>
                        </li>
                        <li>
                            <a href="">Manage Widgets</a>
                        </li>
                        <li>
                            <a href="">Widgets Settings</a>
                        </li>
                    </ul>
                </li>
            </ul>

        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h2>Visitas no site <small>Contador de visitas no site</small></h2>
                    <div class="card-body">
                    {{getTotalVisits()}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-md-offset-1">
            <div class="card">
                <div class="card-header">
                    <h2>Sales Statistics <small>Vestibulum purus quam scelerisque, mollis nonummy metus</small></h2>

                    <ul class="actions">
                        <li>
                            <a href="">
                                <i class="zmdi zmdi-refresh-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="zmdi zmdi-download"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="">Change Date Range</a>
                                </li>
                                <li>
                                    <a href="">Change Graph Type</a>
                                </li>
                                <li>
                                    <a href="">Other Settings</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="card-body">
                    <div class="chart-edge">
                        <div id="curved-line-chart" class="flot-chart "></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mini-charts">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="mini-charts-item bgm-cyan">
                        <div class="clearfix">
                            <div class="chart stats-bar"></div>
                            <div class="count">
                                <small>Total de Publicações</small>
                                <h2>{{getRecentPosts(100)->count()}}</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="mini-charts-item bgm-lightgreen">
                        <div class="clearfix">
                            <div class="chart stats-bar-2"></div>
                            <div class="count">
                                <small>Total de Comentários</small>
                                <h2>{{getRecentComments(100)->count()}}</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="mini-charts-item bgm-orange">
                        <div class="clearfix">
                            <div class="chart stats-line"></div>
                            <div class="count">
                                <small>Total de Visitas</small>
                                <h2>{{getTotalVisits()}}</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="mini-charts-item bgm-bluegray">
                        <div class="clearfix">
                            <div class="chart stats-line-2"></div>
                            <div class="count">
                                <small>Total de Não curtidas</small>
                                <h2>12</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <!-- Recent Posts -->
                <div class="card">
                    <div class="card-header ch-alt m-b-20">
                        <h2>Postagens Recentes</h2>
                        <a href="{{ url('/post/novo') }}" class="btn bgm-cyan btn-float"><i class="zmdi zmdi-plus"></i></a>
                    </div>

                    <div class="card-body">
                        <div class="listview">
                            @foreach(getRecentPosts(5) as $p)
                            <a class="lv-item" href="/post/{{$p->slug}}" target="_blank">
                                <div class="media">
                                    <div class="media-body">
                                        <div class="lv-title">{{$p->titulo_post}}</div>
                                        <small class="lv-small">{!! str_limit($p->txt_post, $limit=80, $end="...") !!}</small>
                                    </div>
                                </div>
                            </a>
                            @endforeach
                            <a class="lv-footer" href="{{ url('/post/lista') }}">Ver Todos</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">     
                <!-- Recent Comments -->
                <div class="card">
                    <div class="card-header ch-alt m-b-20">
                        <h2>Comentários Recentes</h2>
                    </div>

                    <div class="card-body">
                        <div class="listview">
                            @foreach(getRecentComments(5) as $c)
                            <a class="lv-item" href="/post/mostra/{{$c->id}}" target="_blank">
                                <div class="media">
                                    <div class="media-body">
                                        <div class="lv-title">{{$c->nm_comentario}}</div>
                                        <small class="lv-small">{!! str_limit($c->txt_comentario, $limit=80, $end="...") !!}</small>
                                    </div>
                                </div>
                            </a>
                            @endforeach
                            <a class="lv-footer" href="{{ url('/comment/lista') }}">Ver todos</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection