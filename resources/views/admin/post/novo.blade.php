@extends('layouts.dash')

@section('content')
<section id="content">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2>Nova Publicação<small>Formulario para a publicação de uma nova postagem.</small></h2>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                </div>
            @endif
            <div class="card-body card-padding">
                <div class="row">
                    <div class="col-md-10">
                        <!--Form -->
                        <form action="/post/adiciona" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                           <div class="form-group">
                                <label>Imagem</label>
                                <input type="file" name="imagem_post" required>
                            </div>
                            <div class="form-group">
                                <label>Categoria</label>
                                <select id="categoria_id" name="categoria_id" class="form-control" required>
                                    <option>Selecionar</option>
                                    @foreach($categorias as $key => $categoria)
                                        <option value="{{ $key }}">{{ $categoria }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" style='display:none'>
                                <label class="control-label" for="textinput">Vereador: </label>
                                    <select name="vereador_id" id='vereador_id' class="form-control" ></select>
                            </div>
                            <div class="form-group">
                                <label>Titulo da Postagem</label>
                                <input class="form-control" 
                                       id = "id_titulo_post"
                                       name="titulo_post" 
                                       placeholder="Digite o Titulo" 
                                       value="{{ old('titulo_post') }}"
                                       maxlength="300" 
                                       onblur="pegarSlug()"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Caminho da Postagem</label>
                                <input class="form-control" 
                                       id="idSlug"
                                       name="slug" 
                                       placeholder="" 
                                       value="{{ old('slug') }}"
                                       maxlength="350" 
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Subtítulo da Postagem</label>
                                <input class="form-control" 
                                       name="sub_post" 
                                       placeholder="Digite o Subtítulo" 
                                       value="{{ old('sub_post') }}"
                                       maxlength="500" 
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Destaque da Postagem</label>
                                <input class="form-control" 
                                       name="destaque_post" 
                                       placeholder="Digite o Destaque"
                                       value="{{ old('destaque_post') }}"
                                       maxlength="144" 
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="idtexto">Publicação:</label>
                                <textarea name="txt_post">{{ old('txt_post') }}</textarea>
                            </div>

                            <button class="btn btn-danger">Cancelar</button>
                            <button name="rascunho" 
                                    type="submit" 
                                    class="btn btn-primary">
                                    Rascunho
                            </button>
                            <button name="publicar" 
                                    type="submit" 
                                    class="btn btn-success">
                                    Publicar
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(`#categoria_id`).on(`change`,function(e){
        console.log(e);
        var cat_id = e.target.value;

        //ajax
        $.get('/ajax-subcat?cat_id=' + cat_id, function(data){
            //sucess data
            $('#vereador_id').empty();
            $.each(data, function(index, subcatObj){
                $('#vereador_id').append('<option value="'+subcatObj.id+'">'+subcatObj.nm_vereadores+'</option>')
                $('#vereador_id').closest('div.form-group').show('slow');
            });
        }); 
    });
</script>
<script>
    function pegarSlug() {
        var slug = $("#id_titulo_post").val();
        $("#idSlug").val(slug.replace(/[\s]+/g, '-').replace(/[^a-zA-Z0-9\-]+/g, ""));
    }
</script>
@endsection
  
<script type="text/javascript" src='//cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
    tinymce.init({  
        selector: 'textarea',  
        menu: {},
        auto_focus: 'txt_post',
        height: 300,
        plugins: [
            'advlist autolink textcolor lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | forecolor | backcolor | alignleft aligncenter                      alignright  alignjustify | bullist numlist outdent indent | link',
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
</script>

