@extends('layouts.dash')

@section('content')
<section id="content">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2>Editar Publicação<small>Formulario para editar a publicação de uma postagem.</small></h2>
            </div>
            <div class="card-body card-padding">
                <div class="row">
                    <div class="col-md-10">
                        <!--Form -->
                        <form action="/post/salvar/{{$post->id}}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                           <div class="form-group">
                                <label>Imagem</label>
                                <input type="file" 
                                       name="imagem_post" 
                                       value="{{$post->imagem_post}}">
                            </div>
                            <div class="form-group">
                                <label>Categoria</label>
                                <select id="categoria_post" name="categoria_id" class="form-control" required>
                                  @foreach($categorias as $key => $categoria)
                                        <option value="{{$key}}" {{ $post->id != $key }} ?: 'selected' >{{ $categoria }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Titulo da Postagem</label>
                                <input class="form-control" 
                                       name="titulo_post" 
                                       id = "id_titulo_post"
                                       placeholder="Digite o texto" 
                                       value="{{$post->titulo_post}}"
                                       maxlength="300" 
                                       onblur="pegarSlug()"
                                       required>
                            </div>
                            <div class="form-group">
                            <div class="form-group">
                                <label>Caminho da Postagem</label>
                                <input class="form-control" 
                                       id="idSlug"
                                       name="slug" 
                                       placeholder="" 
                                       value="{{$post->slug}}"
                                       maxlength="350" 
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Subtítulo da Postagem</label>
                                <input class="form-control" 
                                       name="sub_post" 
                                       placeholder="Digite o Subtítulo" 
                                       value="{{$post->sub_post}}"
                                       maxlength="500" 
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Destaque da Postagem</label>
                                <input class="form-control" 
                                       name="destaque_post" 
                                       placeholder="Digite o Destaque" 
                                       value="{{$post->destaque_post}}"
                                       maxlength="144" 
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="idtexto">Publicação:</label>
                                <textarea 
                                          class="form-control" 
                                          name="txt_post" 
                                          placeholder="Digite o texto" 
                                          rows="7" 
                                          required>{{$post->txt_post}}</textarea>
                            </div>
                            
                            <button type="button" class="btn btn-danger">Cancelar</button>
                            <button type="button" class="btn btn-primary">Rascunho</button>
                            <button type="submit" class="btn btn-success">Publicar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function pegarSlug() {
        var slug = $("#id_titulo_post").val();
        $("#idSlug").val(slug.replace(/[\s]+/g, '-').replace(/[^a-zA-Z0-9\-]+/g, ""));
    }
</script>
@endsection
<script type="text/javascript" src='//cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
    tinymce.init({  
        selector: 'textarea',  
        menu: {},
        auto_focus: 'txt_post',
        height: 300,
        plugins: [
            'advlist autolink textcolor lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | forecolor | backcolor | alignleft aligncenter                      alignright  alignjustify | bullist numlist outdent indent | link',
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
</script>