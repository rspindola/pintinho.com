@extends('layouts.dash')
@section('content')
<section id="content">
    <div class="container-fluid">
        <div class="card"> 
            @if(empty($posts))
            <div class="alert alert-danger">
                Você não tem nenhuma publicação.
            </div>
            @else           
            <div class="col-md-offset-1 col-md-10">
                <div class="card-header">
                    <h2>Listagem de Publicações <small>Listagem com todas as publicações postadas.</small></h2>
                </div>
                <div class="table-responsive">
                    <table id="data-table-basic" class="table table-striped">
                        <thead>
                            <tr>
                                <th data-column-id="id" data-type="numeric">Titulo</th>
                                <th data-column-id="sender">Texto</th>
                                <th data-column-id="sender">Usuario</th>
                                <th data-column-id="sender">Categoria</th>
                                <th data-column-id="data" data-order="desc">Data</th>
                                <th>Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $p)
                            <tr>
                                <td>{{ str_limit($p->titulo_post, $limit = 30, $end = '...') }}</td>
                                <td>{!! str_limit($p->txt_post, $limit = 30, $end = '...') !!}</td>
                                <td>{{$p->user->name}}</td>
                                <td>{{$p->categoria->nm_categoria}}</td>
                                <td>{{$p->created_at->format("d/m/Y à\s H:i:s")}}</td>
                                <td class="text-center"> 
                                    <a href="/post/{{$p->slug}}"> 
                                        <i class="zmdi zmdi-search zmdi-hc-fw"></i>
                                    </a> 
                                    @if($p->user_id == Auth::id())
                                    <a href="/post/editar/{{$p->id}}"> 
                                        <i class="zmdi zmdi-edit zmdi-hc-fw"></i>
                                    </a>
                                    <a data-id="{{ $p->id }}" class="delete-post">
                                            <i class="zmdi zmdi-delete zmdi-hc-fw"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="delete-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalLabel">Excluir Item</h4>
          </div>
          <div class="modal-body">
            Deseja realmente excluir este item?
          </div>
          <div class="modal-footer">
            <a id="botao-modal" href="/post/remove/{{$p->id}}" type="button" class="btn btn-primary">Sim</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
          </div>
        </div>
    </div>
</div> 
<script>
    $('.delete-post').click(function(){ 
    
    $('#botao-modal').prop('href', "/post/remove/" + $(this).data('id'));
        $("#delete-modal").modal();
    }); 
</script>
@endif
<!-- /.modal -->
@endsection

